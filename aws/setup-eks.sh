#!/bin/bash

# Configuration variables
CLUSTER_NAME="fleetman"
REGION="us-west-2"  # Remplacez par votre région AWS
EKS_VERSION="1.21.0"  # Remplacez par la version EKS par défaut que vous souhaitez utiliser

# Étape 1: Installer eksctl
install_eksctl() {
    echo "Installing eksctl..."
    curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
    sudo mv /tmp/eksctl /usr/local/bin
}

# Étape 2: Mettre à jour AWS CLI vers la version 2
install_aws_cli_v2() {
    echo "Updating AWS CLI to version 2..."
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm awscliv2.zip
    rm -rf aws
}

# Étape 3: Configurer le groupe IAM et la politique
setup_iam_group() {
    echo "Setting up IAM group and policy..."
    aws iam create-group --group-name eks-admins
    aws iam attach-group-policy --group-name eks-admins --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess
    aws iam attach-group-policy --group-name eks-admins --policy-arn arn:aws:iam::aws:policy/IAMFullAccess
    aws iam attach-group-policy --group-name eks-admins --policy-arn arn:aws:iam::aws:policy/AWSCloudFormationFullAccess
    cat <<EOF > inline-policy.json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": "eks:*",
          "Resource": "*"
        },
        {
          "Action": [
            "ssm:GetParameter",
            "ssm:GetParameters"
          ],
          "Resource": "*",
          "Effect": "Allow"
        }
      ]
    }
EOF
    aws iam put-group-policy --group-name eks-admins --policy-name eks-inline-policy --policy-document file://inline-policy.json
    rm inline-policy.json
}

# Étape 4: Ajouter un utilisateur au groupe IAM
add_user_to_iam_group() {
    echo "Adding user to IAM group..."
    aws iam create-user --user-name eks-user
    aws iam add-user-to-group --user-name eks-user --group-name eks-admins
    aws iam create-access-key --user-name eks-user > credentials.json
    export AWS_ACCESS_KEY_ID=$(jq -r '.AccessKey.AccessKeyId' < credentials.json)
    export AWS_SECRET_ACCESS_KEY=$(jq -r '.AccessKey.SecretAccessKey' < credentials.json)
    rm credentials.json
    aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    aws configure set region $REGION
}

# Étape 5: Installer kubectl
install_kubectl() {
    echo "Installing kubectl..."
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v$EKS_VERSION/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
}

# Étape 6: Créer le cluster EKS
create_eks_cluster() {
    echo "Creating EKS cluster..."
    eksctl create cluster --name $CLUSTER_NAME --region $REGION --nodes-min=3
}

# Étape 7: Activer l'accès EBS pour EKS
enable_ebs_access() {
    echo "Enabling EBS access for EKS..."
    eksctl utils associate-iam-oidc-provider --region=$REGION --cluster=$CLUSTER_NAME --approve
    eksctl create iamserviceaccount --name ebs-csi-controller-sa --namespace kube-system --cluster $CLUSTER_NAME --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy --approve --role-only --role-name AmazonEKS_EBS_CSI_DriverRole
    eksctl create addon --name aws-ebs-csi-driver --cluster $CLUSTER_NAME --service-account-role-arn arn:aws:iam::$(aws sts get-caller-identity --query Account --output text):role/AmazonEKS_EBS_CSI_DriverRole --force
}

# Main script execution
main() {
    install_eksctl
    install_aws_cli_v2
    setup_iam_group
    add_user_to_iam_group
    install_kubectl
    create_eks_cluster
    enable_ebs_access
    echo "EKS cluster setup complete. Don't forget to delete the cluster when finished."
}

main "$@"
