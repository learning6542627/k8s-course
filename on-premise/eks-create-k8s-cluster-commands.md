## Warning
You **MUST** delete your cluster when finished to avoid unnecessary charges:
```bash
eksctl delete cluster --name fleetman
```

---

## Step-by-Step Guide to Creating an EKS Cluster

### Step 0: Create an EC2 Instance
1. **Log in to your AWS Management Console.**
2. **Launch a new EC2 instance:**
   - Choose an Amazon Machine Image (AMI), such as Amazon Linux 2.
   - Select an instance type (e.g., `t2.micro` for testing purposes).
   - Configure the instance with at least 8 GB of disk space.
   - Ensure the instance has a public IP if you need to access it from the internet.
3. **Configure security group rules:**
   - Allow SSH access (port 22) from your IP.
   - Allow necessary outbound traffic for internet access.

### Step 1: Install `eksctl`
Download and install `eksctl`:
```bash
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
```

### Step 2: Update AWS CLI to Version 2
Download and install AWS CLI version 2:
```bash
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
Log out of your shell and log back in to ensure the AWS CLI is updated.

### Step 3: Set Up an IAM Group
Set up an IAM group with the following permissions:
- AmazonEC2FullAccess
- IAMFullAccess
- AWSCloudFormationFullAccess

Create an inline policy for the group with the following JSON:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "eks:*",
      "Resource": "*"
    },
    {
      "Action": [
        "ssm:GetParameter",
        "ssm:GetParameters"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
```

### Step 4: Add a User to the Group
Use the AWS Management Console to add a user to your new group. Then configure the AWS CLI with the user’s credentials:
```bash
aws configure
```

### Step 5: Install `kubectl`
**Warning**: Ensure you install a version of `kubectl` that matches the default Kubernetes version supplied with EKS.

1. Export the EKS version number:
    ```bash
    export RELEASE=<enter default eks version number here. Eg 1.17.0>
    ```

2. Download and install `kubectl`:
    ```bash
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v$RELEASE/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```

3. Verify the installation:
    ```bash
    kubectl version --client
    ```

### Step 6: Create Your EKS Cluster
Create the EKS cluster using `eksctl`:
```bash
eksctl create cluster --name fleetman --nodes-min=3
```

### Step 7: Enable EBS for Your EKS Cluster

1. **Associate IAM OIDC provider:**
   ```bash
   eksctl utils associate-iam-oidc-provider --region=ca-central-1 --cluster=fleetman --approve
   ```

2. **Create IAM service account for EBS CSI driver:**
   ```bash
   eksctl create iamserviceaccount --name ebs-csi-controller-sa --namespace kube-system --cluster fleetman --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy --approve --role-only --role-name AmazonEKS_EBS_CSI_DriverRole
   ```

3. **Create EBS CSI driver addon:**
   ```bash
   eksctl create addon --name aws-ebs-csi-driver --cluster fleetman --service-account-role-arn arn:aws:iam::$(aws sts get-caller-identity --query Account --output text):role/AmazonEKS_EBS_CSI_DriverRole --force
   ```

---

### Deleting the Cluster
Once you are finished, delete the cluster to avoid any charges:
```bash
eksctl delete cluster --name fleetman
```

delete the volumes 
stop the bootsrap

This guide provides a step-by-step approach to setting up an EKS cluster using `eksctl`. Make sure to follow each step carefully and delete the cluster when you're done to avoid unnecessary costs.